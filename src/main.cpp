//
// Created by Antoine Duez on 12/10/19.
//

#include <sol/sol.hpp>

int main() {
    sol::state lua;
    lua.open_libraries(sol::lib::base);

    lua.script_file("print('bark bark bark!')");
    return 0;
}