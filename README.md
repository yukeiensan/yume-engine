# Global informations
Engine is in C++ under SDL, the main goal is to do a simple 2d game engine that have an Lua API interface

Game are made in Lua, using the 2d engine API to actually work (I based my project structure on the valve's source engine)

# Conan remote sources
 - bincrafters: https://api.bintray.com/conan/bincrafters/public-conan [Verify SSL: True]
 - conan-transit: https://api.bintray.com/conan/conan/conan-transit [Verify SSL: True]
 
# Known issues

## OSX
 - math.h is providing compile error: check that CPLUS_INCLUDE_PATH is not defined in your environment
 
# Credits
- Antoine 'Yukeien' Duez ~ yukeiensan@gmail.com